#include <iostream>
#include <set>
#include <thread>
#include <mutex>
#include <vector>
#include <cassert>
#include <omp.h>
#include <functional>

typedef std::set<int>(*func_type)(std::set<int>&, std::set<int>&, std::set<int>&, int);

#define GET_FUNC_WITH_NAME(f) \
	std::pair<func_type, std::string>(f, #f)

std::set<int> simple_set_intersection(std::set<int>& first,
						 	 	 std::set<int>& second,
								 std::set<int>& third,
								 int threadAmount) {
	std::set<int> result;
	std::mutex iterator_mutex;
	std::mutex result_mutex;

	auto iterator = first.begin();

	std::vector<std::thread> threads;
	auto end = first.cend();

	for (int i = 0; i < threadAmount; ++i) {
		threads.emplace_back(
				std::thread([&result, &iterator_mutex, &result_mutex, &iterator, end, &second, &third](){
			while (true) {
				int el;
				{
					iterator_mutex.lock();
					if (iterator == end) {
						iterator_mutex.unlock();
						return;
					}
					el = *iterator;
					++iterator;
					iterator_mutex.unlock();
				}

				if (second.find(el) != second.end() and third.find(el) != third.end()) {
					result_mutex.lock();
					result.insert(el);
					result_mutex.unlock();
				}
			}

		}));
	}

	for (std::thread& t : threads) {
		t.join();
	}

	return result;
}


std::set<int> iterator_thread_intersection(std::set<int>& first,
						 	 	 std::set<int>& second,
								 std::set<int>& third,
								 int threadAmount) {
	std::set<int> result;
	std::mutex result_mutex;
	std::vector<std::thread> threads;
	int first_max_element = *(--first.end());
	int second_max_element = *(--second.end());
	int third_max_element = *(--third.end());

	for (int i = 0; i < threadAmount; ++i) {
		auto firstIterator = first.lower_bound(first_max_element / threadAmount * i);
		auto firstIteratorintnd = first.lower_bound(first_max_element / threadAmount * (i + 1) + threadAmount);

		auto secondIterator = second.lower_bound(second_max_element / threadAmount * i);
		auto secondIteratorintnd = second.lower_bound(second_max_element / threadAmount * (i + 1) + threadAmount);

		auto thirdIterator = third.lower_bound(third_max_element / threadAmount * i);
		auto thirdIteratorintnd = third.lower_bound(third_max_element / threadAmount * (i + 1) + threadAmount);


		threads.emplace_back(
				std::thread([&result, &result_mutex](
						typename std::set<int>::iterator firstIterator,
						typename std::set<int>::iterator firstIteratorintnd,
						typename std::set<int>::iterator secondIterator,
						typename std::set<int>::iterator secondIteratorintnd,
						typename std::set<int>::iterator thirdIterator,
						typename std::set<int>::iterator thirdIteratorintnd) {
			while (firstIterator != firstIteratorintnd and secondIterator != secondIteratorintnd
					and thirdIterator != thirdIteratorintnd) {
				if (*firstIterator > *secondIterator) {
					++secondIterator;
					continue;
				}

				if (*firstIterator < *secondIterator) {
					++firstIterator;
					continue;
				}

				if (*firstIterator > *thirdIterator) {
					++thirdIterator;
					continue;
				}

				if (*firstIterator < *thirdIterator) {
					++firstIterator;
					continue;
				}

				result_mutex.lock();
				result.insert(*firstIterator);
				result_mutex.unlock();

				++firstIterator;
				++secondIterator;
				++thirdIterator;
			}
		}, firstIterator, firstIteratorintnd, secondIterator, secondIteratorintnd,
		       thirdIterator, thirdIteratorintnd));
	}

	for (std::thread& t : threads) {
		t.join();
	}

	return result;
}


std::set<int> iterator_thread_intersection_openMP(std::set<int>& first,
						 	 	 std::set<int>& second,
								 std::set<int>& third,
								 int threadAmount) {
	std::set<int> result;

	int first_max_element = *(--first.end());
	int second_max_element = *(--second.end());
	int third_max_element = *(--third.end());

	#pragma omp parallel num_threads(threadAmount) shared(result)
	{
		assert(omp_get_num_threads() == threadAmount);
		int thread_id = omp_get_thread_num();
		auto firstIterator = first.lower_bound(first_max_element / threadAmount * thread_id);
		auto firstIteratorintnd = first.lower_bound(first_max_element
				/ threadAmount * (thread_id + 1) + threadAmount);

		auto secondIterator = second.lower_bound(second_max_element / threadAmount * thread_id);
		auto secondIteratorintnd = second.lower_bound(second_max_element
				/ threadAmount * (thread_id + 1) + threadAmount);

		auto thirdIterator = third.lower_bound(third_max_element / threadAmount * thread_id);
		auto thirdIteratorintnd = third.lower_bound(third_max_element
				/ threadAmount * (thread_id + 1) + threadAmount);

		while (firstIterator != firstIteratorintnd and secondIterator != secondIteratorintnd
					and thirdIterator != thirdIteratorintnd) {
			if (*firstIterator > *secondIterator) {
				++secondIterator;
				continue;
			}

			if (*firstIterator < *secondIterator) {
				++firstIterator;
				continue;
			}

			if (*firstIterator > *thirdIterator) {
				++thirdIterator;
				continue;
			}

			if (*firstIterator < *thirdIterator) {
				++firstIterator;
				continue;
			}

			#pragma omp critical
			{
				result.insert(*firstIterator);
			}

			++firstIterator;
			++secondIterator;
			++thirdIterator;
		}
	}

	return result;
}


std::set<int> iterator_thread_intersection_split_result(std::set<int>& first,
						 	 	 std::set<int>& second,
								 std::set<int>& third,
								 int threadAmount) {
	std::vector<std::set<int>> result_tmp(threadAmount);
	std::vector<std::thread> threads;
	int first_max_element = *(--first.end());
	int second_max_element = *(--second.end());
	int third_max_element = *(--third.end());

	for (int i = 0; i < threadAmount; ++i) {
		auto firstIterator = first.lower_bound(first_max_element / threadAmount * i);
		auto firstIteratorintnd = first.lower_bound(first_max_element / threadAmount * (i + 1) + threadAmount);

		auto secondIterator = second.lower_bound(second_max_element / threadAmount * i);
		auto secondIteratorintnd = second.lower_bound(second_max_element / threadAmount * (i + 1) + threadAmount);

		auto thirdIterator = third.lower_bound(third_max_element / threadAmount * i);
		auto thirdIteratorintnd = third.lower_bound(third_max_element / threadAmount * (i + 1) + threadAmount);

		threads.emplace_back(
				std::thread([](
						typename std::set<int>::iterator firstIterator,
						typename std::set<int>::iterator firstIteratorintnd,
						typename std::set<int>::iterator secondIterator,
						typename std::set<int>::iterator secondIteratorintnd,
						typename std::set<int>::iterator thirdIterator,
						typename std::set<int>::iterator thirdIteratorintnd,
						std::set<int> *result) {
			while (firstIterator != firstIteratorintnd and secondIterator != secondIteratorintnd
					and thirdIterator != thirdIteratorintnd) {
				if (*firstIterator > *secondIterator) {
					++secondIterator;
					continue;
				}

				if (*firstIterator < *secondIterator) {
					++firstIterator;
					continue;
				}

				if (*firstIterator > *thirdIterator) {
					++thirdIterator;
					continue;
				}

				if (*firstIterator < *thirdIterator) {
					++firstIterator;
					continue;
				}

				result->insert(*firstIterator);

				++firstIterator;
				++secondIterator;
				++thirdIterator;
			}
		}, firstIterator, firstIteratorintnd, secondIterator, secondIteratorintnd, thirdIterator,
			thirdIteratorintnd, &result_tmp[i]));
	}

	for (std::thread& t : threads) {
		t.join();
	}

	std::set<int> result;
	for (int i = 0; i < threadAmount; ++i) {
	    result.insert(result_tmp[i].begin(), result_tmp[i].end());
	}
	return result;
}


std::set<int> iterator_thread_intersection_split_result_openMP(std::set<int>& first,
						 	 	 std::set<int>& second,
								 std::set<int>& third,
								 int threadAmount) {
	std::vector<std::set<int>> result_tmp(threadAmount);

	int first_max_element = *(--first.end());
	int second_max_element = *(--second.end());
	int third_max_element = *(--third.end());

	#pragma omp parallel num_threads(threadAmount) shared(result_tmp)
	{
		assert(omp_get_num_threads() == threadAmount);
		int thread_id = omp_get_thread_num();
		auto firstIterator = first.lower_bound(first_max_element / threadAmount * thread_id);
		auto firstIteratorintnd = first.lower_bound(first_max_element
				/ threadAmount * (thread_id + 1) + threadAmount);

		auto secondIterator = second.lower_bound(second_max_element / threadAmount * thread_id);
		auto secondIteratorintnd = second.lower_bound(second_max_element
				/ threadAmount * (thread_id + 1) + threadAmount);

		auto thirdIterator = third.lower_bound(third_max_element / threadAmount * thread_id);
		auto thirdIteratorintnd = third.lower_bound(third_max_element
				/ threadAmount * (thread_id + 1) + threadAmount);

		while (firstIterator != firstIteratorintnd and secondIterator != secondIteratorintnd
				and thirdIterator != thirdIteratorintnd) {
			if (*firstIterator > *secondIterator) {
				++secondIterator;
				continue;
			}

			if (*firstIterator < *secondIterator) {
				++firstIterator;
				continue;
			}

			if (*firstIterator > *thirdIterator) {
				++thirdIterator;
				continue;
			}

			if (*firstIterator < *thirdIterator) {
				++firstIterator;
				continue;
			}

			result_tmp[thread_id].insert(*firstIterator);

			++firstIterator;
			++secondIterator;
			++thirdIterator;
		}
	}


	std::set<int> result;
	for (int i = 0; i < threadAmount; ++i) {
	    result.insert(result_tmp[i].begin(), result_tmp[i].end());
	}
	return result;
}



void test_function(std::function<std::set<int>(std::set<int>&, std::set<int>&, std::set<int>&, int)> f,
		std::set<int>& first, std::set<int>& second, std::set<int>& third, int threads, std::string func_name) {
    using std::chrono::high_resolution_clock;
    using std::chrono::duration_cast;
    using std::chrono::duration;
    using std::chrono::milliseconds;

    auto t1 = high_resolution_clock::now();
    f(first, second, third, threads);
    auto t2 = high_resolution_clock::now();


    auto ms_int = duration_cast<milliseconds>(t2 - t1);
    std::cout << func_name << " with " << threads << " threads took: " << ms_int.count() << "ms\n";
}


int main() {
	std::set<int> first;
	std::set<int> second;
	std::set<int> third;
	int set_size = 3000000;

	for (int i = 0, value = 0; i < set_size; i++, value += 2) {
		first.insert(value);
	}

	for (int i = 0, value = 0; i < set_size; i++, value += 5) {
		second.insert(value);
	}

	for (int i = 0, value = 0; i < set_size; i++, value += 11) {
		third.insert(value);
	}


	int threads[]{1, 2, 4, 8, 16, 32, 64, 128, 256, 1024};

	std::pair<func_type, std::string>
		functions_to_test[]
			{GET_FUNC_WITH_NAME(simple_set_intersection),
			 GET_FUNC_WITH_NAME(iterator_thread_intersection),
			 GET_FUNC_WITH_NAME(iterator_thread_intersection_openMP),
			 GET_FUNC_WITH_NAME(iterator_thread_intersection_split_result),
			 GET_FUNC_WITH_NAME(iterator_thread_intersection_split_result_openMP)};

	for (auto &f : functions_to_test) {
		for (int threads : threads) {
			test_function(std::function<std::set<int>(std::set<int>&, std::set<int>&,
					std::set<int>&, int)>(f.first),
					first, second, third, threads, f.second);
		}
	}
}
